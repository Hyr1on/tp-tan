import requests

api_key = "7f36f962c1f793aa9fce8f55acde64f7c85595b545843d18ab54af55"  # Remplacez par votre clé d'API de la TAN
stop_id = "HODI"  # Remplacez par l'identifiant de l'arrêt que vous souhaitez interroger

# URL de l'API de la TAN pour les horaires des arrêts
url = f"https://api.tan.fr/schedule/station/{stop_id}?key={api_key}"

# Effectuer la requête GET à l'API
response = requests.get(url)

# Vérifier si la requête a réussi (code de statut 200)
if response.status_code == 200:
    # Récupérer les données de réponse au format JSON
    data = response.json()
    # Faire quelque chose avec les données (par exemple, les afficher)
    print(data)
else:
    print("La requête a échoué.")


    