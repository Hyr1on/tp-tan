import json
import folium
from folium.plugins import MarkerCluster

# Initialisation de la map
nantes = folium.Map(location=[47.2181, -1.5528], zoom_start=12)
# disableClusteringAtZoom : pour désactiver l'effet de package quand on serapproche trop
mc = MarkerCluster(disableClusteringAtZoom=15).add_to(nantes)

# Ouverture du fichier tan arrets. 
with open('tan-arrets.json') as file:
    data_a = json.load(file) 

# Ouverture du fichier tan circuits. 
with open('tan-circuits.json') as file:
    data_c = json.load(file)

# Ouverture du fichier tan circuits. 
with open('correspondance.json') as file:
    data_co = json.load(file)

#function d'ajout des lignes avec la ligne et les directions.
def addLine(list_coo, routeID, colorID, shortname, longname):
    ligne_popup = f"<b>Ligne:</b> {shortname}<br>"
    ligne_popup += f"<b>Vers:</b> {longname} <br>"
    # on ajout les coordonnées pour créer la ligne tout en créant une inversion entre lat/long
    folium.PolyLine(locations=[(lat, lng) for lng, lat in list_coo], color="#"+colorID, popup=ligne_popup).add_to(mc)

## Ajout  des arrêts dans la map
for feature in data_a["features"]:
    stop_Coo_Twice = feature['geometry']['coordinates']
    stop_ID = feature['properties']['stop_id']
    stop_Name = feature['properties']['stop_name']
    stop_ltype = feature['properties']['location_type']
    stop_wheelchair_boarding = feature['properties']['wheelchair_boarding']

    bus_popup = f"<b>Arrêt: {stop_Name}<br>"
    if stop_wheelchair_boarding == '1' or stop_wheelchair_boarding == '2':
        bus_popup += f"<b>Accès handicapé: Oui <br>" 
    else:
        bus_popup += f"<b>Accès handicapé: Non <br>"
        
    parent_id = feature['properties']['parent_station']

    if stop_Name == 'Ile de Nantes':
        bus_popup += f"<img src='image-ile.png' alt='Ile de Nantes' style='width: 400%;'>"

    # for cor in data_co:
    #     codeLieu = cor["codeLieu"]
    #     ci=0
    #     print(cor["ligne"])
    #     if codeLieu == stop_ID
    #     # try:ct
    #     #     if ci <= len(cor['ligne']):
    #     #         #print(cor['ligne'][ci])
    #     #         # bus_popup += f"<b>Ligne: {cor['ligne'][ci]['numLigne']} <br>"
    #     #         print(cor['ligne'][ci])
    #     #         ci=ci+1
    #     #     else:
    #     #         pass
    #     # except:
    #     #     pass

     # # Créer un marqueur pour l'arrêt de bus avec des métadonnées
    folium.Marker([stop_Coo_Twice[1], stop_Coo_Twice[0]], icon=folium.Icon(icon="train", prefix="fa",color="green"), popup = bus_popup).add_to(mc)

## Ajout des lignes dans la map
## Enregistrement des infos utile. 
for ligne in data_c:
    route_ID = ligne['route_id']
    route_short_name = ligne['route_short_name']
    route_long_name = ligne['route_long_name']
    route_type = ligne["route_type"]
    route_color = ligne['route_color']

    # ajout des lignes avec les coo, en inversant long/lat 
    # # l'ajout ce fait via la fonction addLine, 
    # # qui nous permets de la rappeler autant de fois que nous avons de trajet différentes par lignes.
    i=0
    while i <= len(ligne['shape']['geometry']["coordinates"]):
        try:
            addLine(ligne['shape']['geometry']["coordinates"][i], route_ID, route_color, route_short_name, route_long_name)
        except:
            pass
        i=i+1

# on ajoute le tout à la map
nantes.add_child(mc)
# on extract la map en html, pour la consulter.
nantes.save('tan-nantes.html')
nantes

# Pour l'ajout des horaires
# l'api n'est malheureusement pas la même que pour les datas utilisés.
# https://plan-tan.fr/referentiel/getstoptimetable?stopId=731&lineId=37&direction=1&date=08/06/2023
# donc l'id à spéficier n'est pas le même